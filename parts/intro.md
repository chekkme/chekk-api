# Introduction
__Chekk-Api__ is a SaaS product that allows the control and request of the Chekk platform. 

It offers the possibility of leveraging the platform features programmatically and interchangeably, as the API and the web apps will offer the same features and communicate with the same backend with the same workflow.  

Hence, it will allow third party developer to integrate Chekk technology in their own products, without impacting the end-users. 

Do not hesitate to [contact us](mailto:developers@chekk.me) for feature requests, support directly from our developers as well as implementation in other technologies that could suit your needs.

Chekk-Portal is a platform that allows the management, sharing and validation of personal and official documents. It allows Chekk customers to create campaigns of millions of users with a single click (enquiries, validation, automatic reminders).

This API is a white label, flexible and 'à la carte' version of this technology and makes it easy for Chekk customers to develop their own customer journey experience. 

In this document, the following terms refer to: 

* __Client:__  
the Chekk-Portal customer (eg: you and your company) 

* __Enterprise:__  
An entity that is an enterprise 

* __Individual:__  
A human entity that is a retail customer or related to an enterprise 

 
# How to connect to our API
To be able to use the Chekk services, the following is required:
 - __1__: a Cognito token (which is mandatory to access our cloud)
 - __2__: a Chekk API token (which holds the encryption keys)
 - __3__: a call of an API function with both tokens

## 1 - Get a Cognito token
Connect to our cloud with an OAuth2 authentication. 

 Please refer to the <a href='#section/Authentication/Cognito'>Cognito</a> section.


## 2 - Get a Chekk API token (Cognito token required)
Personal and business data is encrypted on Chekk servers using the user's own keys. Chekk is a zero knowledge provider. As such, to use the Chekk services and have access to the data on Chekk server, connecting to the Chekk API with the same login as on Chekk Portal is required.

 Please refer to the <a href='#tag/Basic'>Login</a> section.


## 3 -  Call an API function (Cognito token and Chekk API token are required))
When connected to the Chekk Cloud via Cognito and ChekkAPI, including both Cognito and Chekk API tokens in the header is mandatory to make API calls.
 
For example, please see the following template to get an account detail call.

 > ```curl -i -X POST https://pdwjyr93n9.execute-api.eu-west-1.amazonaws.com/prod/data/my/details  -H 'X-Authorization: Bearer {Cognito_token}' -H 'Authorization: Bearer {ChekkAPIToken}' -H 'Content-Type: application/json' ``` 

 {Cognito_token} is replaced by the access_token from CognitoToken. 

 {ChekkAPIToken} is replaced by the access_token from ChekkAPIToken. 
 
 Here is an example:
 > ```curl -i -X POST https://pdwjyr93n9.execute-api.eu-west-1.amazonaws.com/prod/data/my/details -H 'X-Authorization: Bearer eyD[...]QDA' -H 'Authorization: Bearer eyJ[...]vm0' -H 'Content-Type: application/json'``` 

 # Error management 
For example, please see the following template to get an account detail call. In the following table is a description of the first part of every error code in Chekk. 

 | Prefix | Description      | Info                                               |
|--------|------------------|----------------------------------------------------|
| MFD    | Missing field    | A mandatory field is missing                       |
| IFD    | Invalid field    | A field is not well formed                         |
| FER    | Functional error | Email already exists, API call pending... |
| IER    | Internal error | Internal error! Contact us immediately... |

For example, an error code starting with \"MFD_\" means there is a missing field in the current call.
Any code starting with IER means there is an issue on Chekk side.
Error codes starting with IFD or FER can be returned as is in the context of an integration of the Chekk API into third party services.
"

