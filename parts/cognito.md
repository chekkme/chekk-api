To get the Cognito token, you need to use get it from a special endpoint by using the ```curl``` below
\n
```javascript
curl -X POST   https://chekkrec.auth.eu-west-1.amazoncognito.com/oauth2/token?grant_type=client_credentials&scope=api/data api/services'   -H 'Authorization: Basic {account_token}’   -H 'Content-Type: application/x-www-form-urlencoded'
```

\n\n
In input you insert the account given token in place of ‘{account_token}’ (no {} in the final call around this parameter).

For Linklogis tests (pre-production) account, here is the account token to use:

```javascript
account_token: MzhydWFlN2FiZjk1ZGw2cjBtNGNqMWZubTY6MWN2aXVkb2ZhYWxkbzhwNnBoMDVyZGxmamNjZ2VjdmVuMzcxOTBwOWMxbjlmNnZua2lpcw==
```

\n\n
The return of the ‘Cognito API call has the following format:

```json
{
"access_token":"access_token",

"expires_in":3600,

"token_type":"Bearer"
}
```

\n\n
The Cognito token is returned in the ```access_token``` field and you need to use it for all the following API call requests.

\n\n
Log in to the Chekk API plateform using the Chekk Portal credentials.\n\nYou will get a return in the following format: 
```json
{"username": "username","roles": ["MANAGER"],"token_type":"Bearer", "access_token": "chekk Api token",  "expires_in": 3600}
```
Retrieve the value in “access_token”. It is the Chekk API token. Both the Chekk API token and the Cognito Token are required to use other API functions."