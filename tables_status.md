### faceMatchingEnroll, faceMatchingVerify,faceMatchingRecognize
 | code | message                         |
 |------|---------------------------------|
 | 400  | Image is mandatory              |
 | 400  | Subject_id is mandatory long |
 
 ### faceMatchingDetect
 | code | message                         |
 |------|---------------------------------|
 | 400  | Image is mandatory              |
 
 ### faceMAtchesWith
  | code | message                         |
  |------|---------------------------------|
  | 400  | Image is mandatory              |
  | 400  | Comparator is mandatory |
  | 400  | Illegal comparator |
  | 400  | Email is mandatory |
  | 400  | No user found |
  

### infiogreffeId, infogreffeCapital, InfogreffeRepresentatives, infogreffeAllData
### verifyEntity, verifyRepresentatives
| code | message                         |
|------|---------------------------------|
| 400  | siren is mandatory              |
| 400  | Siren must be 9 characters long and only contain numbers |

### infogreffeAccounts
| code | message                         |
|------|---------------------------------|
| 400  | Siren is mandatory              |
| 400  | Siren must be 9 characters long and only contain numbers |
| 400  | Year is mandatory              |
| 400  | Year must be yyyy |
| 404  | Bilan non trouvé |

### resolveEntity
| code | message                         |
|------|---------------------------------|
| 400  | Entity name or code is mandatory|
| 400  | SearchType is mandatory|
| 400  | Illegal SearchType |
| 400  | SearchMode is mandatory|
| 400  | Illegal SearchMode |

### crawlEntity
| code | message                         |
|------|---------------------------------|
| 400  | Entity name or code is mandatory              |
| 400  | Jurisdiction is mandatory |

### verifyEntity
| code | message                         |
|------|---------------------------------|
| 400  | Illegal searchType|
| 400  | Entity name or code is mandatory|
| 400  | FirstName is mandatory |
| 400  | Lastname is mandatory |


### KbisTriangulation()
| code | message                         |
|------|---------------------------------|
| 400  | Siren is mandatory              |
| 400  | Siren must be 9 characters long and only contain numbers |
| 400  | Document is mandatory           |
| 400  | triangulationType is mandatory|
| 400  | Illegal triangulationType     |

### ocr()
| code | message                         |
|------|---------------------------------|
| 400  | Document is mandatory              |
| 400  | frontDocumentType (PNG / JPG...) is mandatory |

### ocrAml()
| code | message                         |
|------|---------------------------------|
| 400  | Document is mandatory              |
| 400  | frontDocumentType (PNG / JPG...) is mandatory |

### ocrAmlDataTriangulation()
| code | message                         |
|------|---------------------------------|
| 400  | Document is mandatory              |
| 400  | FrontDocumentType (PNG / JPG...) is mandatory |
| 400  | Firstname is mandatory |
| 400  | LastName is mandatory |
| 400  | Dob is mandatory |
| 400  | Address is mandatory |

### ocrAmlResult
| code | message                         |
|------|---------------------------------|
| 400  | InitialOCRQueryRestart is mandatory |
| 400  | Source request not found |
| 400  | Not completed |

## ocrResult()
| code | message                         |
|------|---------------------------------|
| 400  | InitialOCRQueryRestart is mandatory |



## SscrabberKbis, scrapperStatus
| code | message                         |
|------|---------------------------------|
| 400  | siren is mandatory              |
| 400  | Siren must be 9 characters long and only contain numbers |
| 400  | No document found for siren     |

## Result ApiCall
| code | message                         |
|------|---------------------------------|
| 400  | Token is mandatory              |
| 400  | Token is malformed |
| 400  | Not completed     |