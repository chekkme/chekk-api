#### FaceMatchingEnroll
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_35_13|image is mandatory|
|400|MFD_35_29|Subject_id is mandatory|

#### FaceMatchingVerify
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_36_13|image is mandatory|
|400|MFD_36_29|Subject_id is mandatory|

#### FaceMatchingRecognize
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_37_13|image is mandatory|
|400|MFD_37_29|Subject_id is mandatory|

#### FaceMatchesWith
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_34_13|image is mandatory|
|400|MFD_34_3|comparator is mandatory|
|400|MFD_34_7|email is mandatory|
|400|IFD_34_6|Email is not valid|
|400|FER_34_2|No user found|
|400|IFD_34_7|Illegal comparator, must be either one of [ID, PASSPORT, ALL]|
|400|FER_34_3|No data fround for provided user|

#### FaceDetect
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_33_13|image is mandatory|

#### InfogreffeId
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_6_20|siren is mandatory|
|400|IFD_6_8|Siren must be 9 characters long and only contain numbers|
|400|FER_6_16|Siren does not exists|


#### InfogreffeRepresentatives
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_11_20|siren is mandatory|
|400|IFD_11_8|Siren must be 9 characters long and only contain numbers|

#### InfogreffeCapital
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_12_20|siren is mandatory|
|400|IFD_12_8|Siren must be 9 characters long and only contain numbers|

#### InfogreffeAccount
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_13_20|siren is mandatory|
|400|IFD_13_8|Siren must be 9 characters long and only contain numbers|
|400|MFD_13_25|year is mandatory|
|400|IFD_13_9|Year must be provided as numeric YYYY|
|400|FER_13_17|Account not found|

#### InfogreffeAllData
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_14_20|siren is mandatory|
|400|IFD_14_8|Siren must be 9 characters long and only contain numbers|

#### InfogreffeVerifyEntity
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_3_20|siren is mandatory|
|400|IFD_3_8|Siren must be 9 characters long and only contain numbers|

#### InfogreffeVerifyRepresentatives
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_4_20|siren is mandatory|
|400|IFD_4_8|Siren must be 9 characters long and only contain numbers|

#### AcraEntity
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_48_37|UEN is mandatory|
|400|IFD_48_24|Uen must be 10 characters long|

#### Kbistriangulation
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_5_20|siren is mandatory|
|400|IFD_5_8|Siren must be 9 characters long and only contain numbers|
|400|MFD_5_6|document is mandatory|
|400|MFD_5_33|triangulationType is mandatory|
|400|IFD_5_21|Invalid triangulationType, must be either one of [BASIC, MEDIUM, ADVANCED]|

#### VerifyPerson
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_39_28|Email or UUID is mandatory|
|400|MFD_39_28|Email or UUID is mandatory|
|400|FER_39_5|No person found for provided email|

#### VerifyIndividual
| status | code | message                         |
|--------|------|---------------------------------|
|400|FER_38_4|No individual found for provided information|
|400|FER_38_6|No enterprise found for provided information|

#### VerifyEnterprise
| status | code | message                         |
|--------|------|---------------------------------|
|400|FER_40_6|No enterprise found for provided information|

#### Ocr
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_43_6|document is mandatory|
|400|MFD_43_11|front document type (jpg/png...) is mandatory|
|400|MFD_43_23|type of document is mandatory|
|400|MFD_43_4|ISO 3166-1 (3 letters) country document is mandatory|

#### OcrResult
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_41_14|initialOCRQueryRestart is mandatory|

#### OcrAml
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_42_6|document is mandatory|
|400|MFD_42_11|front document type (jpg/png...) is mandatory|
|400|MFD_42_23|type of document is mandatory|
|400|MFD_42_4|ISO 3166-1 (3 letters) country document is mandatory|

#### OcrAmlDataTriangulation
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_7_6|document is mandatory|
|400|MFD_7_11|front document type (jpg/png...) is mandatory|
|400|MFD_7_23|type of document is mandatory|
|400|MFD_7_4|ISO 3166-1 (3 letters) country document is mandatory|
|400|MFD_7_26|Query body is mandatory|
|400|MFD_7_27|IdDocument body is mandatory|
|400|MFD_7_34|typeDocProofOfAddress is mandatory|
|400|MFD_7_36|country is mandatory|
|400|MFD_7_16|last name is mandatory|
|400|MFD_7_10|first name is mandatory|
|400|MFD_7_5|date of birth is mandatory|
|400|MFD_7_1|adress is mandatory|

#### OcrAmlResult
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_44_14|initialOCRQueryRestart is mandatory|
|400|FER_44_13|No api call found for provided token|
|400|FER_44_12|API call is still pending|


#### VerifyAml
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_15_17|name is mandatory|
|400|IFD_15_25|Fuzziness must be between 0 and 10|
|400|MFD_15_32|Type is mandatory|
|400|IFD_15_20|Invalid verificationType, must be either one of [PERSON, ENTITY, ALL]|

#### ResolveEntity
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_45_9|entity name or code is mandatory|
|400|MFD_45_30|SearchType is mandatory|
|400|IFD_45_18|Invalid searchType, must be either one of [COMPANY, OFFICER]|
|400|MFD_45_31|SearchMode is mandatory|
|400|IFD_45_19|Invalid searchMode, must be either one of [[SEARCH, FIND_BY]|

#### ScrapperKbis
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_1_20|siren is mandatory|
|400|IFD_1_8|Siren must be 9 characters long and only contain numbers|

#### ScrapperStatus
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_2_20|siren is mandatory|
|400|IFD_2_8|Siren must be 9 characters long and only contain numbers|

#### QueryApiCall
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_46_22|Token is mandatory|
|400|IFD_46_11|Token must be of UUID format|
|500|IER_46_4|Decyrption failed|
|400|FER_46_12|API call is still pending|
|400|FER_46_13|No api call found for provided token|

#### ResultApiCall
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_16_22|Token is mandatory|
|400|IFD_16_11|Token must be of UUID format|
|400|FER_16_12|API call is still pending|

#### Individual invite
| status | code | message                         |
|--------|------|---------------------------------|
|400|FER_31_19|Max invitation reached|
|400|FER_31_18|Already invited|
|400|FER_31_9|No country found for provided code|
|400|FER_31_19|Invalid datarequest|
|400|MFD_25_38|Datarequest is mandatory|
|400|IFD_31_6|Email is not valid|
|400|IFD_31_16|Illegal profile. Use the data/individual/profiles endpoint to find out which values are allowed|

#### Enterprise invite
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_25_38|Datarequest is mandatory|
|400|FER_25_19|Max invitation reached|
|400|FER_25_18|Already invited|
|400|FER_25_19|Invalid datarequest|
|400|FER_25_9|No country found for provided code|
|400|IFD_25_6|Email is not valid|

#### Enterprise invited
| status | code | message                         |
|--------|------|---------------------------------|
|400|MFD_23_9|entity name or code is mandatory|
|400|MFD_23_7|email is mandatory|
|400|IFD_23_6|Email is not valid|
|400|FER_25_19|Max invitation reached|